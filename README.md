#### 招募MAC平台打包志愿者：

作者没有mac，mac打包至今没有进展，招募一名热爱开源的同学帮忙打包mac平台安装包！联系邮箱：maxbill1993@163.com


# RedisPlus

#### 项目介绍

RedisPlus是为Redis管理开发的桌面客户端软件，支持Windows 、Linux 、Mac三大系统平台，RedisPlus提供更加高效、方便、快捷的使用体验，有着更加现代化的界面风格。该软件参考了RedisStudio的界面逻辑，但是和RedisStudio软件完全没有关系，并不是官方推出的跨平台软件。该软件为开源免费使用（可商用），但是禁止二次开发打包发布盈利，违反必究！ 该软件遵循GPL开源协议，致力于为大家提供一个高效的Redis可视化管理软件。

#### 下载地址

1.微云地址：https://share.weiyun.com/5UIOsxY

![输入图片说明](https://images.gitee.com/uploads/images/2018/0909/200121_021d84e3_1252126.png "屏幕截图.png")


#### 版本说明

版本的命名规则以x.y.z形式，重大版本更新发布会更改x位主版本，例如v2.0.0 版本会增加集群、支持ssh通道连接、国际化等功能；一般解决了多个缺陷后，会发布一个小版本，更改y位，例如v1.1.0解决了1.0.0 的问题后发布的小版本；z位的更改不会发布版本，这是缺陷修复或者小需求增加的正常迭代。

#### 版本规划

v1.1.0

1.美化实时监控页面(图表配色，单位……)   （已完成）

2.增加更新日志功能

3.增加版本规划功能

4.新增异常处理页面  （已完成）

5.数据模块key分层显示

6.数据显示vlaue值反序列化，不乱码处理

7.状态栏连接状态美化(加图标)    （已完成）

8.增加使用教程

9.增加全局按key模糊查询  （已完成）

10.key过多加载卡死问题修复   （已修复，可处理百万级别）

11.打包windows平台安装包  （已完成）

12.打包mac平台安装包

12.修复1.0.0问题，性能优化（持续进行）


v2.0.0

1.SSH通道支持

2.集群连接支持

3.支持国际化


#### 软件交流

1.QQ群：857111033  点击链接加入群聊【RedisPlusj交流群】：https://jq.qq.com/?_wv=1027&k=5nFw9eg

2.issues专区：https://gitee.com/MaxBill/RedisPlus/issues
 

#### 技术选型

1.支持跨平台，使用java开发

2.使用javafx的桌面元素

3.使用derby内嵌数据库

4.内置服务使用springboot开发

#### 应用截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174302_dfd839b5_1252126.png "深度截图_com.maxbill.MainApplication_20180904174001.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174311_9991de81_1252126.png "深度截图_com.maxbill.MainApplication_20180904174021.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174321_53591727_1252126.png "深度截图_com.maxbill.MainApplication_20180904174037.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174331_3baf9be8_1252126.png "深度截图_com.maxbill.MainApplication_20180904174051.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174341_dcfb9cb9_1252126.png "深度截图_com.maxbill.MainApplication_20180904174106.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0904/174352_3738f4fe_1252126.png "深度截图_com.maxbill.MainApplication_20180904174116.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0907/123200_ed3b67c9_1252126.png "深度截图_com.maxbill.MainApplication_20180907123037.png")
