package com.maxbill.base.bean;

import lombok.Data;

@Data
public class Connect {

    private String id;

    private String name;

    private String host;

    private String port;

    private String pass;

    private String time;
}
