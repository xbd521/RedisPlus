package com.maxbill.base.bean;

import lombok.Data;

@Data
public class DataTable {

    private Integer code;

    private String msgs;

    private Integer count;

    private Object data;

    private Object param;
}
